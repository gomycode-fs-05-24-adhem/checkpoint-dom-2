
const colorDiv = document.querySelector('.box-color')
const button = document.querySelector('.button')

function getRandomColor() {
    const registre = "012345789azertyqsdf"
    let couleur =  '#'
    //Apparemment, ici, il faut toujours que i soit inférieur à 6 car la couleur hexadécimale s'écrit avec six caractères.
    for (let i = 0; i < 6 ; i++) {
        couleur = couleur + registre[Math.floor(Math.random() * 10)];
    }
    return couleur
}

button.addEventListener('click', function () {
    colorDiv.style.backgroundColor = getRandomColor()
})